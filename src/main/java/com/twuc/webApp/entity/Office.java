package com.twuc.webApp.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Office {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;
//    cascade = CascadeType.ALL,nullable = false
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "officeId")
    private List<Staff> staffs = new ArrayList<>();

    public Office() {
    }

    public Office(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Staff> getStaffs() {
        return staffs;
    }

    public void addStaff(Staff staff){
        staffs.add(staff);
    }
}
