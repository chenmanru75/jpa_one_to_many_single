package com.twuc.webApp;

import com.twuc.webApp.entity.Office;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.repository.OfficeRepository;
import com.twuc.webApp.repository.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class WebAppApplicationTest {

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private EntityManager entityManager;

    private void flushAndClear(Runnable run) {
        run.run();
        entityManager.flush();
        entityManager.clear();
    }


    //如果@OneToMany没有设置级联,在存完office时，office状态变为cache
    //而由于没有级联staff状态为dacache状态，
    //entityManager的flush只会扫描到cache状态的office（flush只会扫描到cache状态的实例），所以执行失败
    //object references an unsaved transient instance - save the transient instance before flushing: com.twuc.webApp.entity.Staff
    //测试的时候把cascade删除
    @Test
    void no_cascade() {

        Staff staff = new Staff("Manru");
        Office office = new Office("Beijing");
        office.addStaff(staff);
        officeRepository.save(office);
        entityManager.flush();
        entityManager.clear();

    }


    //如果@OneToMany设置级联，在存完office时，office状态变为cache，staff状态也为cache状态，
    //此时entityManager的flush会扫描到cache状态的office和staff
    //所以存储成功，但是这种方法会先insert office ,再insert staff, 最后updata staff的office_id

    //一对多设置级联，需要三次数据库操作，实现级联更新两个表
    @Test
    void cascade() {
        Staff staff = new Staff("Manru");
        Office office = new Office("Beijing");
        office.addStaff(staff);
        officeRepository.save(office);
        entityManager.flush();
        entityManager.clear();

        assertEquals(1, staffRepository.findAll().size());
    }

    //office加上@JoinColumn告诉staff是控制关系的实例
    //操作控制关系的实例相关的都会更新，例如staffRepository.save(staff); staff表和office表会同时更新，存储成功。??????
    //并且这种方法只会insert staff表一次
    //更推荐这种做法 （相比于级联 ，更推荐操作控制关系的实例，级联和控制关系没有任何关系）

    @Test
    void reference() {
        Staff staff = new Staff("Manru");
        Office office = new Office("Beijing");
        office.addStaff(staff);
        staffRepository.save(staff);
//        officeRepository.save(office);
        entityManager.flush();
        entityManager.clear();
//        int size = office.getStaffs().size();
        int size = staffRepository.findAll().size(); //如果加上nullable为false  测试过不了
        assertEquals(1, size);
    }

    @Test
    void delete_office() {
        Staff staff = new Staff("Manru");
        Office office = new Office("Beijing");
        office.addStaff(staff);
        officeRepository.save(office);
        entityManager.flush();
        entityManager.clear();
        officeRepository.deleteById(1L);
        assertEquals(0,officeRepository.findAll().size());
    }
//
//    @Test
//    void name3() {
//        Staff staff = new Staff("Manru");
//        Office office = new Office("Beijing");
//        office.addStaff(staff);
//        Office save = officeRepository.save(office);
////        Staff save = staffRepository.save(staff);
//        entityManager.flush();
//        entityManager.clear();
//        int size = office.getStaffs().size();
////        assertEquals(1, save.getStaffs().size());
//        assertEquals(1, size);
//    }
}